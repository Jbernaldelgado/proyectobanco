package co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.cuenta;

import co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.cuenta.CuentaData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CuentaDataJpaRepository extends JpaRepository<CuentaData, Long> {
}
