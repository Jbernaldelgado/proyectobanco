package co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.movimiento;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MovimientoDataJpaRepository extends JpaRepository<MovimientoData, Long> {
}
