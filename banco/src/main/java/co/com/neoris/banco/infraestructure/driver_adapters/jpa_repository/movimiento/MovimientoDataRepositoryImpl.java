package co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.movimiento;

import co.com.neoris.banco.domain.model.movimiento.Movimiento;
import co.com.neoris.banco.domain.model.movimiento.gateways.MovimientoGateway;
import co.com.neoris.banco.infraestructure.mapper.MapperMovimiento;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class MovimientoDataRepositoryImpl implements MovimientoGateway {

    private final MapperMovimiento mapperMovimiento;
    private final MovimientoDataJpaRepository movimientoDataJpaRepository;

    @Override
    public Movimiento saveMovimiento(Movimiento movimiento) {
        var data = mapperMovimiento.toData(movimiento);
        return mapperMovimiento.toMovimiento(movimientoDataJpaRepository.save(data));
    }

    @Override
    public void deleteMovimiento(Long id) {
        movimientoDataJpaRepository.deleteById(id);
    }

    @Override
    public Movimiento updateMovimiento(Movimiento movimiento) {
        return null;
    }
}
