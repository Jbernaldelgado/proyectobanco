package co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.cliente;

import co.com.neoris.banco.domain.model.cliente.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteDataJpaRepository extends JpaRepository<ClienteData, Long> {
}
