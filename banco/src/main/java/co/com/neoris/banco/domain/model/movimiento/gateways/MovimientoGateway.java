package co.com.neoris.banco.domain.model.movimiento.gateways;

import co.com.neoris.banco.domain.model.movimiento.Movimiento;

public interface MovimientoGateway {

    Movimiento saveMovimiento(Movimiento movimiento);
    void deleteMovimiento(Long id);
    Movimiento updateMovimiento(Movimiento movimiento);
}
