package co.com.neoris.banco.infraestructure.entry_points;

import co.com.neoris.banco.domain.model.cliente.Cliente;
import co.com.neoris.banco.domain.model.cuenta.Cuenta;
import co.com.neoris.banco.domain.usecase.ClienteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("clientes")
@RequiredArgsConstructor
public class ClienteController {

    private final ClienteUseCase clienteUseCase;

    @PostMapping("")
    public ResponseEntity<Cliente> postSave(@RequestBody Cliente cliente) throws Exception {
        Cliente clienteSave = clienteUseCase.saveCliente(cliente);
        return new ResponseEntity<>(clienteSave, HttpStatus.OK);
    }
}
