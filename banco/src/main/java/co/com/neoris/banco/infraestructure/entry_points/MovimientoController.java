package co.com.neoris.banco.infraestructure.entry_points;

import co.com.neoris.banco.domain.model.cuenta.Cuenta;
import co.com.neoris.banco.domain.model.movimiento.Movimiento;
import co.com.neoris.banco.domain.usecase.MovimientoUseCase;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("movimientos")
@RequiredArgsConstructor
public class MovimientoController {
    private final MovimientoUseCase movimientoUseCase;

    @PostMapping("")
    public ResponseEntity<Movimiento> postSave(@RequestBody Movimiento movimiento) throws Exception {
        Movimiento movimientoSave = movimientoUseCase.saveMovimiento(movimiento);
        return new ResponseEntity<>(movimientoSave, HttpStatus.OK);
    }
}
