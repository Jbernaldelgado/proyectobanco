package co.com.neoris.banco.domain.model.exception;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@ControllerAdvice
public class GlobalErrorHandler {

    @ExceptionHandler(RequestException.class)
    public ResponseEntity<Object> methodArgumentNotValidException(HttpServletRequest request,
                                                                  RequestException exception) {
        HttpError errorInfo = HttpError.builder().message("sesion invalida").build();
        return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
    }
}
