package co.com.neoris.banco.infraestructure.mapper;


import co.com.neoris.banco.domain.model.cliente.Cliente;
import co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.cliente.ClienteData;
import co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.persona.PersonaData;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class MapperCliente {

    public Cliente toCliente(ClienteData clienteData){
        return new Cliente(
                clienteData.getId(),
                clienteData.getClienteId(),
                clienteData.getContrasena(),
                clienteData.getEstado(),
                clienteData.getPersona()
        );
    }

    public ClienteData toData(Cliente cliente){
        return new ClienteData(
                cliente.getId(),
                cliente.getClienteId(),
                cliente.getContrasena(),
                cliente.getEstado(),
                (PersonaData) cliente.getPersona()
        );
    }
}
