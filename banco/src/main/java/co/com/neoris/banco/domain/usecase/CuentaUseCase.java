package co.com.neoris.banco.domain.usecase;

import co.com.neoris.banco.domain.model.cuenta.Cuenta;
import co.com.neoris.banco.domain.model.cuenta.gateways.CuentaGateway;
import co.com.neoris.banco.domain.model.exception.RequestException;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CuentaUseCase {

    private final CuentaGateway cuentaGateway;

    public Cuenta saveCuenta(Cuenta cuenta) throws RequestException {
        if(cuenta.getNumCuenta() == null){
            throw new RequestException("Sesion invalida");
        }
        return cuentaGateway.saveCuenta(cuenta);
    }

    public void deleteCuenta(Long id){
        try{
            cuentaGateway.deleteCuenta(id);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
