package co.com.neoris.banco.domain.usecase;

import co.com.neoris.banco.domain.model.movimiento.Movimiento;
import co.com.neoris.banco.domain.model.movimiento.gateways.MovimientoGateway;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MovimientoUseCase {

    private final MovimientoGateway movimientoGateway;

    public Movimiento saveMovimiento(Movimiento movimiento) throws Exception{
        if(movimiento.getValor() == null){
            throw new Exception();
        }
        return movimientoGateway.saveMovimiento(movimiento);
    }

    public void deleteMovimiento(Long id){
        try{
            movimientoGateway.deleteMovimiento(id);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
