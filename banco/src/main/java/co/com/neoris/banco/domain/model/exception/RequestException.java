package co.com.neoris.banco.domain.model.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestException extends RuntimeException{
    private static final long serialVersionUID = -1741800479810649707L;

    private final String messageId;
    private final String code;
    private final String description;

    public RequestException() {
        super();
        this.messageId = null;
        this.code = null;
        this.description = null;
    }

    public RequestException(String message, Throwable cause) {
        super(message, cause);
        this.messageId = null;
        this.code = null;
        this.description = message;
    }

    public RequestException(String description) {
        super(description);
        this.messageId = null;
        this.code = null;
        this.description = description;
    }

    public RequestException(final Throwable cause) {
        super(cause);
        this.messageId = null;
        this.code = null;
        this.description = null;
    }

    public RequestException(String messageId, String code, String description) {
        super();
        this.messageId = messageId;
        this.code = code;
        this.description = description;
    }

    public RequestException(String code, String description) {
        super();
        messageId = null;
        this.code = code;
        this.description = description;
    }

}
