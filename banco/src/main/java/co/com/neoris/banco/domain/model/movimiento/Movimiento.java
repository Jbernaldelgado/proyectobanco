package co.com.neoris.banco.domain.model.movimiento;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Movimiento {

    private Long id;
    private Date fecha;
    private String tipoMovimiento;
    private Integer valor;
    private Integer saldo;
}
