package co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.cliente;

import co.com.neoris.banco.domain.model.persona.Persona;
import co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.persona.PersonaData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "cliente_tb", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClienteData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String clienteId;

    @Column(nullable = false)
    private String contrasena;

    @Column(length = 1)
    private String estado;

    @ManyToOne
    @JoinColumn(name = "persona")
    private PersonaData persona;
}
