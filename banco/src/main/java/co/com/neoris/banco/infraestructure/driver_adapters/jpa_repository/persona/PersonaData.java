package co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.persona;

import co.com.neoris.banco.domain.model.persona.Persona;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "persona_tb", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PersonaData extends Persona {

    @Id
    private String identificacion;

    @Column
    private String nombre;

    @Column
    private String direccion;

    @Column
    private String genero;

    @Column
    private Integer edad;

    @Column
    private Integer telefono;
}
