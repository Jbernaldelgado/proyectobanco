package co.com.neoris.banco.domain.model.cuenta.gateways;

import co.com.neoris.banco.domain.model.cuenta.Cuenta;

public interface CuentaGateway {

    Cuenta saveCuenta(Cuenta cuenta);
    void deleteCuenta(Long id);
    Cuenta updateCuenta(Cuenta cuenta);
}
