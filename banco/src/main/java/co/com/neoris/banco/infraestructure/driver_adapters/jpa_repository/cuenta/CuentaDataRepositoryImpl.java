package co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.cuenta;

import co.com.neoris.banco.domain.model.cuenta.Cuenta;
import co.com.neoris.banco.domain.model.cuenta.gateways.CuentaGateway;
import co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.cuenta.CuentaDataJpaRepository;
import co.com.neoris.banco.infraestructure.mapper.MapperCuenta;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class CuentaDataRepositoryImpl implements CuentaGateway {

    private final MapperCuenta mapperCuenta;
    private final CuentaDataJpaRepository cuentaDataJpaRepository;

    @Override
    public Cuenta saveCuenta(Cuenta cuenta) {
        var data = mapperCuenta.toData(cuenta);
        return mapperCuenta.toCuenta(cuentaDataJpaRepository.save(data));
    }

    @Override
    public void deleteCuenta(Long id) {
        cuentaDataJpaRepository.deleteById(id);
    }

    @Override
    public Cuenta updateCuenta(Cuenta cuenta) {
        return null;
    }
}
