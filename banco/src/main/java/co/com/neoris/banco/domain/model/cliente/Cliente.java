package co.com.neoris.banco.domain.model.cliente;

import co.com.neoris.banco.domain.model.persona.Persona;
import co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.persona.PersonaData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Cliente {

    private Long id;
    private String clienteId;
    private String contrasena;
    private String estado;
    private Persona persona;

}
