package co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.cliente;

import co.com.neoris.banco.domain.model.cliente.Cliente;
import co.com.neoris.banco.domain.model.cliente.gateways.ClienteGateway;
import co.com.neoris.banco.domain.model.cuenta.Cuenta;
import co.com.neoris.banco.infraestructure.mapper.MapperCliente;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ClienteDataRepositoryImpl implements ClienteGateway {

    private final MapperCliente mapperCliente;
    private final ClienteDataJpaRepository clienteDataJpaRepository;

    @Override
    public Cliente saveCliente(Cliente cliente) {
        var data = mapperCliente.toData(cliente);
        return mapperCliente.toCliente(clienteDataJpaRepository.save(data));
    }

    @Override
    public void deleteCliente(Long id) {
        clienteDataJpaRepository.deleteById(id);
    }

    @Override
    public Cliente updateCliente(Cliente cliente) {
        return null;
    }

}
