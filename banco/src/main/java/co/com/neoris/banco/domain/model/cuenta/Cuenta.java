package co.com.neoris.banco.domain.model.cuenta;

import co.com.neoris.banco.domain.model.persona.Persona;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Cuenta {

    private Long id;
    private String numCuenta;
    private String tipoCuenta;
    private Integer saldoInicial;
    private String estado;

}
