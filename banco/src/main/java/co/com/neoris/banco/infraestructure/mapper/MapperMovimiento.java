package co.com.neoris.banco.infraestructure.mapper;

import co.com.neoris.banco.domain.model.movimiento.Movimiento;
import co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.movimiento.MovimientoData;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class MapperMovimiento {

    public Movimiento toMovimiento(MovimientoData movimientoData){
        return new Movimiento(
                movimientoData.getId(),
                movimientoData.getFecha(),
                movimientoData.getTipoMovimiento(),
                movimientoData.getValor(),
                movimientoData.getSaldo()
        );
    }

    public MovimientoData toData(Movimiento movimiento){
        return new MovimientoData(
                movimiento.getId(),
                movimiento.getFecha(),
                movimiento.getTipoMovimiento(),
                movimiento.getValor(),
                movimiento.getSaldo()
        );
    }
}
