package co.com.neoris.banco.domain.model.persona;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Persona {

    private String identificacion;
    private String nombre;
    private String direccion;
    private String genero;
    private Integer edad;
    private Integer telefono;
}
