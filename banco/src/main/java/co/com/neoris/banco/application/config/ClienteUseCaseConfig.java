package co.com.neoris.banco.application.config;

import co.com.neoris.banco.domain.model.cliente.gateways.ClienteGateway;
import co.com.neoris.banco.domain.usecase.ClienteUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "co.com.neoris.usecase", includeFilters = {
        @ComponentScan.Filter(type = FilterType.REGEX, pattern = "^.+UseCase$")}, useDefaultFilters = false)
public class ClienteUseCaseConfig {

    @Bean
    public ClienteUseCase clienteUseCase(ClienteGateway clienteGateway){
        return new ClienteUseCase(clienteGateway);
    }
}
