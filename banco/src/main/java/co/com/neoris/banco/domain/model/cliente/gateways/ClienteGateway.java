package co.com.neoris.banco.domain.model.cliente.gateways;

import co.com.neoris.banco.domain.model.cliente.Cliente;

public interface ClienteGateway {
    Cliente saveCliente(Cliente cliente);
    void deleteCliente(Long id);
    Cliente updateCliente(Cliente cliente);
}
