package co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.cuenta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "cuenta_tb", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CuentaData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String numCuenta;

    @Column(nullable = false)
    private String tipoCuenta;

    @Column(length = 1)
    private String estado;

    @Column
    private Integer saldoInicial;

}
