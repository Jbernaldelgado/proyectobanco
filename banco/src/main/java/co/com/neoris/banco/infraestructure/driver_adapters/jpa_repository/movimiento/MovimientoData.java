package co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.movimiento;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "movimiento_tb", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MovimientoData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private Date fecha;

    @Column
    private String tipoMovimiento;

    @Column
    private Integer valor;

    @Column
    private Integer saldo;
}
