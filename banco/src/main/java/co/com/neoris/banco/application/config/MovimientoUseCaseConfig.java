package co.com.neoris.banco.application.config;

import co.com.neoris.banco.domain.model.movimiento.gateways.MovimientoGateway;
import co.com.neoris.banco.domain.usecase.MovimientoUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "co.com.neoris.usecase", includeFilters = {
        @ComponentScan.Filter(type = FilterType.REGEX, pattern = "^.+UseCase$")}, useDefaultFilters = false)
public class MovimientoUseCaseConfig {

    @Bean
    public MovimientoUseCase movimientoUseCase(MovimientoGateway movimientoGateway){
        return new MovimientoUseCase(movimientoGateway);
    }
}
