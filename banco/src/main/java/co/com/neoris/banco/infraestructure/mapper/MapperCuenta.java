package co.com.neoris.banco.infraestructure.mapper;

import co.com.neoris.banco.domain.model.cuenta.Cuenta;
import co.com.neoris.banco.infraestructure.driver_adapters.jpa_repository.cuenta.CuentaData;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class MapperCuenta {

    public Cuenta toCuenta(CuentaData cuentaData){
        return new Cuenta(
                cuentaData.getId(),
                cuentaData.getNumCuenta(),
                cuentaData.getTipoCuenta(),
                cuentaData.getSaldoInicial(),
                cuentaData.getEstado()
        );
    }

    public CuentaData toData(Cuenta cuenta){
        return new CuentaData(
                cuenta.getId(),
                cuenta.getNumCuenta(),
                cuenta.getTipoCuenta(),
                cuenta.getEstado(),
                cuenta.getSaldoInicial()
        );
    }
}
