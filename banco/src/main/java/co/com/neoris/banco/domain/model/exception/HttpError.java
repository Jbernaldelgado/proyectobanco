package co.com.neoris.banco.domain.model.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class HttpError {

    private final String message;
}
