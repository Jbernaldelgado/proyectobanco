package co.com.neoris.banco.domain.usecase;

import co.com.neoris.banco.domain.model.cliente.Cliente;
import co.com.neoris.banco.domain.model.cliente.gateways.ClienteGateway;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ClienteUseCase {

    private final ClienteGateway clienteGateway;

    public Cliente saveCliente(Cliente cliente) throws Exception{
        if(cliente.getClienteId() == null){
            throw new Exception();
        }
        return clienteGateway.saveCliente(cliente);
    }

    public void deleteCliente(Long id){
        try{
            clienteGateway.deleteCliente(id);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
