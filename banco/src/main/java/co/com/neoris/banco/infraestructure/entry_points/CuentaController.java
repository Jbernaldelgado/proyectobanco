package co.com.neoris.banco.infraestructure.entry_points;

import co.com.neoris.banco.domain.model.cuenta.Cuenta;
import co.com.neoris.banco.domain.model.exception.RequestException;
import co.com.neoris.banco.domain.usecase.CuentaUseCase;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("cuentas")
@RequiredArgsConstructor
public class CuentaController {
    private final CuentaUseCase cuentaUseCase;

    @PostMapping("")
    public ResponseEntity<Cuenta> postSave(@RequestBody Cuenta cuenta) throws RequestException {
        Cuenta cuentaSave = cuentaUseCase.saveCuenta(cuenta);
        return new ResponseEntity<>(cuentaSave, HttpStatus.OK);
    }
}
