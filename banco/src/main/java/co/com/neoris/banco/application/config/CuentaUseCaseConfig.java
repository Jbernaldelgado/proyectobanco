package co.com.neoris.banco.application.config;

import co.com.neoris.banco.domain.model.cliente.gateways.ClienteGateway;
import co.com.neoris.banco.domain.model.cuenta.gateways.CuentaGateway;
import co.com.neoris.banco.domain.model.movimiento.gateways.MovimientoGateway;
import co.com.neoris.banco.domain.usecase.ClienteUseCase;
import co.com.neoris.banco.domain.usecase.CuentaUseCase;
import co.com.neoris.banco.domain.usecase.MovimientoUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "co.com.neoris.usecase", includeFilters = {
        @ComponentScan.Filter(type = FilterType.REGEX, pattern = "^.+UseCase$")}, useDefaultFilters = false)
public class CuentaUseCaseConfig {

    @Bean
    public CuentaUseCase cuentaUseCase(CuentaGateway cuentaGateway){
        return new CuentaUseCase(cuentaGateway);
    }

}
