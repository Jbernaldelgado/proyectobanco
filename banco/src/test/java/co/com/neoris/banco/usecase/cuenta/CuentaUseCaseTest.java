package co.com.neoris.banco.usecase.cuenta;

import co.com.neoris.banco.domain.model.cuenta.Cuenta;
import co.com.neoris.banco.domain.model.cuenta.gateways.CuentaGateway;
import co.com.neoris.banco.domain.usecase.CuentaUseCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CuentaUseCaseTest {

    private Cuenta cuentaData;
    private Cuenta cuentaResponse = new Cuenta();

    @Mock
    private CuentaUseCase cuentaUseCase;

    @Mock
    private CuentaGateway cuentaGateway;

    @Before
    public void setup() {

        cuentaData = Cuenta.builder().
                numCuenta("12345").
                tipoCuenta("corriente").
                estado("A").
                saldoInicial(12345).
                build();
    }

    @Test
    public void postSaveTest(){
        cuentaUseCase.saveCuenta(cuentaData);
        verify(cuentaGateway, times(1)).saveCuenta(cuentaData);
    }
}
